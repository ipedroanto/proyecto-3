// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-destino-detalle',
//   templateUrl: './destino-detalle.component.html',
//   styleUrls: ['./destino-detalle.component.css']
// })
// export class DestinoDetalleComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from '../Models/destino-viaje.model';
import { DestinosApiClient } from '../Models/destinos-api-client.model';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.scss']
})
export class DestinoDetalleComponent implements OnInit {

  destino: DestinoViaje;

  constructor(private router: ActivatedRoute, private destinoApiClient: DestinosApiClient) { }

  ngOnInit(): void {
    const id = this.router.snapshot.paramMap.get('id');
    console.log(id);
    this.destino = null;
    // this.destino = this.destinoApiClient.getById(id);
  }

}